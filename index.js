'use strict';

class OOPocketApiGateway {
    constructor(serverless, options) {
        this.serverless = serverless;
        this.custom = this.serverless.service.custom;
        this.options = options;
        this.provider = this.serverless.getProvider(this.serverless.service.provider.name);
        this.hooks = {
            'after:deploy:deploy': this.afterDeployDeploy.bind(this),
        };
    }

    resolveRestApiId() {
        return this.provider.request('APIGateway', 'getRestApis', { limit: 500 })
            .then((res) => {
                if (res.items.length) {
                    const filteredRestApis = res.items.filter((api) =>
                        api.name === `${this.options.stage}-${this.serverless.service.service}`);
                    if (filteredRestApis.length) {
                        const restApi = filteredRestApis.shift();
                        return restApi.id;
                    }
                }
                return null;
            })
            .then((restApiId) => {
                this.apiGatewayRestApiId = restApiId;
            });
    }

    hasOoPocketApiGatewayLogLevel() {
        if (this.custom && this.custom.oopocket && this.custom.oopocket.apiGateway && this.custom.oopocket.apiGateway.logLevel) {
            return true;
        }
        return false;
    }

    afterDeployDeploy() {
        if (this.hasOoPocketApiGatewayLogLevel() === false) {
            return;
        }

        return this.resolveRestApiId().then(() => {
            if (['INFO', 'ERROR'].includes(this.custom.oopocket.apiGateway.logLevel)) {
                let patchOperations = [];
                let logLevel = { op: 'replace', path: '/*/*/logging/loglevel', value: 'ERROR' };
                let dataTrace = { op: 'replace', path: '/*/*/logging/dataTrace', value: 'true' };
                patchOperations.push(logLevel);
                patchOperations.push(dataTrace);
                let restApiId = this.apiGatewayRestApiId;
                this.provider.request('APIGateway', 'updateStage', {
                     restApiId,
                     stageName: this.options.stage,
                     patchOperations,
                });
            }
        });
    }
}

module.exports = OOPocketApiGateway;
